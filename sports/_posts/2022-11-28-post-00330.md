---
layout: post
title: "[공문선] 개복치로 살아남기 - 오늘도 유리멘탈로 힘들어하는 당신에게"
toc: true
---

 저자 : 공문선
출판 : 스밈
출간 : 2021.08.09

       

 개복치 계열 책들이 읽어보고 싶어져서 몇 권을 골랐는데, 표지가 귀여워서 선택했다가 깊은 내상을 입었다. 일단 완독은 했지만...

 

 그럭저럭 저자가 의도했던 것은 쉽게 상처받고 괴로워하는 현대인들의 사모 건강에 관서 치유와 위로의 메세지를 전하는 것이었던 걸로 보인다. 그것을 위해 유명인들의 사례나 사회/심리 실험의 결과들을 인용해서 풀어나가는데, 문제는 그쪽 사례나 결과들이 슬쩍 부정확하게 인용되었다는 점들이다. 발췌에서는 누락했지만 링컨의 경우라거나 용여 인물들에 관해서도 지나치게 단편적인 부분들을 슬쩍하다 설명하고 있어서, 전후 맥락을 아는 상태에서 읽으면 당황스러운 부분들이 많았다.

 

 서사 전체를 묶어주는 '공통되고 일관된 관점'의 부재가 밖주인 큰 문제였다. 여존 비문과 오류들은 지엽적인 문제다. 혹시 저자는 처음부터 경계 권의 책을 염두에 두고 글을 썼던 것은 아닌 것 같고, 특정 사건이나 책 등에 관한 짧은 글들을 써두었다가 모아서 엮은 것 같다.
 

 벽 권 안에서 저자가 정의하는 '멘탈'은 엄청 다양한 의미로 사용된다. 아무개 챕터에서는 강한 멘탈을 긍정하고, 또 다른 챕터에서는 도리어 부정한다. 이것은 통로 글을 쓸 때마다 저자가 '멘탈'이라는 용어의 정의를 다르게 잡고 접근했기 때문이다. 조각글들의 탄원 글을 쓴 시점이 제각기 다르고 군 문서 안에서 말하고자 하는 바들도 다르므로 관점이나 용어정리가 흐트러질 수 있는데, 책으로 엮을 계기 조금 더한층 글순이나 수정에 신경을 썼더라면 더욱더 통일성있게 느껴지지 않았을까 싶다.

 

 그런 상태에서 부정확하게 요약/인용된 바깥양반 사실들이 덧씌워져서- 읽는 일거리 내상을 상당히 입었다. 저자가 주장하는 '개복치는 온혈 동물이다'라는 내용은 전혀 다른 어종에 관한 연구를 (붉평치를 붉은 개복치로도 부르는데 번역상의 표시 오류라고 할 삶 있다. 그것을 개복치로 착각한 듯 하다.) 실조 인용한 것이다. 연구자에 관한 내용들을 아울러 표기했기에 금세 사항 여부를 확인할 요행 있다는 것은 장점이지만, 간단한 확인으로도 오류/비약이 발견되는데 별도의 민담 궁핍히 근본적 내용으로 사용한 점은 무척 아쉽다.

 (이 책은 "개복치"에 관한 책이고, 연결 주장을 통해 사실은 "개복치"도 자신만의 장점이 있으니 멘탈 '개복치'로 사는 것도 괜찮다는 내용이었다.)
 

 이식 책을 읽으면서 느낀 불편한 지점들은 현실 내가 냄새 블로그에 써놓은 글들을 묶어서 읽을 기후 느낄 복수 있는 불편함과 같으리라 본다. 나 더구나 계속해서 생각이 바뀌고 있으므로 + 글을 쓰는 시점에 따라, 강조하고 싶은 내용에 따라 입장이 모순적이어질 성명 있으므로 누군가에게는 비슷한 느낌을 주리라 생각한다. 발췌만 해두었을 뿐 사실관계 확인을 족다리 못한 부분도 많다. 그러면 눈치 스스로에 관련해서도 고민해봐야 할 부분이다.

 

 ... 힘들었다. 

 

       

   

 

 

 

 

 

 

 - 영화 중축 주인공처럼 대단한 사람이 아니더라도 우리는 모두 멘붕을 극복할 고갱이 있다. 자신의 멘탈에 귀둘레 기울이고, 멘탈에 대한 통제력을 가진다면 누구나 강한 멘탈을 조카 생명 있다. 피할 행운 없는 멘붕을 이겨내는 경험을 매우 가질수록 우리는 누구 멘붕이 와도 이겨내는 자신감을 유자 요행 있고, 강한 멘탈의 소유자가 될 행우 있을 것이다.
 

 - 멘탈이 약한 개복치도 상어가 대체 따라올 요행 없는 기술을 가지고 있다. 신기하게도 개복치는 냉혈 동물인 다른 물고기들과 달리 온몸에 따뜻한 피를 가진 온혈 동물이라고 한다. 요마적 미국 국립 해양대기청 니콜라스 웨그너 박사의 연구에 따르면 심해에 사는 물고기들은 체온이 낮아 대요 느릿느릿하게 움직이는데 세상없이 움직이면 에너지를 쓰고, 체온조절이 어려워지기 때문이다. 그럼에도 불구하고 개복치는 따뜻한 피가 흐르고 있어서 다른 심해어들과 달리 신속히 움직일 운명 있고 사냥을 하거나 상어 같은 포식자를 손해 빠르게 도망 다닐 운 있다고 한다.
(리뷰자 기근 : 온혈 동물인 개복치는 '붉은 개복치'로, 정확하게는 '붉평치(Opah, Lampris guttatus)'다. 안타깝지만 붉평치는 붉평치과/속이므로 개복치과/속인 개복치와는 무척 다른 종이다. Nicholas C. Wegner의 발표는 Opah에 관한 것이었다.)
 

 - 멘탈이 강하고 성공한 사람들은 이러한 안티프래질 그만 멘탈을 보호하는 뽁뽁이를 자못 가지고 있다. 류현진 선수는 멘탈이 강하기로 유명하다. 안티 프래질 이론에 따르면 류선수는 멘탈이 강한 것이 아니라 제출물로 멘탈을 보호하고 강화시킨 것이다. 만손 풍습 150km 던지는 류현진 선수라 할지라도 상대편의 홈런을 맞으면 멘탈이 부서질 복수 있다. 속으론 그 누구보다 속상하고 답답하겠지만 겉으로는 티를 또는 않으려고 한다. 누구보다 답답하고 절망적이겠지만 이들은 자신의 생각이나, 말이 자신이 인제 처한 상황에 어떠한 영향을 미칠지 너무도 즉속 알고 있기에 태도를 더한층 긍정적으로 바꿔 부서지지 않으려 애쓴다.

 (리뷰자 바탕 : 음... 류현진 선수는 홈런을 맞아서 부서지는 게 아니라... 아마도... 행복수비... 직접 건 알아서 잘...)
 

 - 입스에 걸리거나 슬럼프에 빠지거나 번아웃이 오는 것 모두가 몸에 문제가 있어 발생하는 증상이 아니라 멘탈이 흔들리고 약해지고 지쳐서 오는 멘탈 장애의 일종이다. 이러한 멘탈 장애들을 극복하려면 누구나 이러한 장애에 겪을 행복 있다는 사실을 인정하고 원인을 찾고 극복할 명 있는 방법을 찾아야 한다. 불안해하거나 초조해하거나 조바심을 내기보다는 자신의 멘탈을 돌아보는 기회로 삼고 장애를 극복하는 드릴 과정을 통해 자신의 멘탈을 한결 단단하고 강하게 만드는 계기로 만들 행복 있어야 한다.
 

 - 불가피한 멘붕에 빠지면 당연히 변 또한 지나가리라는 흔들리지 않는 믿음을 지녀야 하지만, 당장 좋아지겠지 하는 막연한 희망보다는 냉혹한 사실들을 직시하고 해결하고자 하는 노역 과약 함께 기울여야 한다.

 

 - 체력이 전염된다고 하면 누구도 믿기 어렵겠지만, 전미 경제조사국의 2010년 보고서에 따르면 약한 체력은 미 공군사관학교에서 힘껏 전염병처럼 퍼졌다고 한다. 장부 적응을 못하는 부적격자의 체력은 다른 후보생의 도움 수준을 초초 저하시켰던 것이다.
 (리뷰자 밑바탕 : 동료가 포기하는 모습을 보는 것이 교련 강도에 영향을 미쳤을 수는 있겠지만... 견련 내용은 확인이 필요하다.)

- 생텍쥐페리가 쓴 <아리스로의 비행>이라는 책에 보면 용기가 없는 사람은 다른 사람의 용기마저 빼앗아 버린다고 했다. 행복도 마찬가지다. 이즈막 하버드대 니콜라스 크리스타키스 박사의 연구 결과에 따르면 행복이 가족, 친구, 가근방 등 지인은 상의물론 깊숙이 모르는 사람에까지 퍼져 나간다는 사실이 확인됐다. 

- 주변에 나의 멘탈을 약하게 만드는 사람이나 환경이 없는지 언젠가 살펴볼 필요가 있다. "네가 뭘 할 복수 있겠어? 내가 볼 때는 힘들다고 봐..." 이런 부정적이고 약한 멘탈을 가진 사람들 그렇게 눈치 멘탈이 약해지는 것은 아닌지 살펴볼 필요가 있다. 그들과 과감히 멀어지고 강하고 긍정적인 멘탈을 가진 사람들 곁으로 가라.
 

 - 우리 사회는 지나친 긍정주의로 인해 우울, 냉담, 문제 제기 등을 나쁜 것이라고 몰아가는 경향이 있다. 힘든 일이 생기거나 슬픈 감정이 야외 때면, 그것을 내보이는 것을 강박적으로 회피하고 괜찮은 척하라고 강요당하고 있다. 스스로의 감정을 십분 돌보지 않는 상황들이 반복되면서 현대인들에게 멘탈이 붕괴돼 우울증이나 공황장애 등이 사회문제로 나타나는 것이 아닐까 싶다. 멘탈이 강해지려면 무조건적인 긍정보다는 이성적으로 더구나 현실성 있게 상황과 상태를 직시하도록 자신을 훈련시키고 균형을 잡는 것이 중요하다. 긍정이 나쁜 것일지라도 그대로 좋게 받아들이라는 의미가 절대로 아니기 때문이다. 즉, 긍정은 모든 걸 좋다고 생각하는 것이 절대 아니고 진정한 긍정은 일단 나에게 일어난 상황을 인정하고 그다음 해결책을 찾는 것이다.
 

 - 긍정적인 생각을 가지는 것이 잘못된 것은 아니지만 긍정적인 생각을 강요하는 것은 무언가 불합리한 것마저 놓칠 위험이 있다. 즉변 될 수 있다는 생각은 심리적으로 안정을 주기도 반면에 늘 뭐든지 좋은 결과를 가져다 줄 거라는 믿음으로 연결되어서는 처 된다.
 

 - 눈물이 많은 사람을 우리는 울보라고 놀리고 울보들은 나약하고 성숙하지 못하다고 말한다. 반면 눈물은 자신의 감정을 두려워하지 않는다는 것을 뜻한다. 우리는 행복할 기간 미소를 짓고, 놀랐을 때는 놀란 표정을 짓는다. 그렇기는 해도 슬플 기후 눈물을 흘리는 감정에 대해서만 표현하기를 두려워한다. 반면에 눈물은 자연스러운 인간의 셈 묘출 방법일 뿐이다. 울고 싶을 촌수 우는 사람들은 자신의 감정을 인정하는 것이고, 자신을 표현할 운명 있는 용기가 있는 사람들이고 자신의 기분을 통제할 힘을 가진 강한 사람들이다. 반면 정신적으로 건강하며 용감한 사람들인 셈이다. 울음은 변형력 해소에도 도움이 된다. 실제 눈물은 스트레스를 유발하는 호르몬인 카테콜아민을 배출하는 역할을 구하 때문이다.

 

 - 멘탈저하상태가 되면 노지 환경에도 취약해지지만 보다 큰 문제가 특정 상황에 대한 판단과 시선이 오로지 자신에게로 향한다는 특징이 있다. 모든 문제의 기인 자신에게 있으며 자신이 잘못해서 이런 상황이나 결과가 발생했다고 자신을 지나치게 책망한다.
 

 - 멘탈저하상태가 되면 모 감정을 느낄 목숨 없는 상태가 된다. 자신이 무얼 좋아하는지 또 싫어하는지 알 행운 없다. 또한, 감정이 없으니 모모 것도 결정하지 못한다. 우울증에 걸린 사람들이 무기력한 상태, 무감동 상태를 호소하는 이유는 그들이 멘탈 저하 상태에 머물러 있기 때문이다.
 

 - 멘탈저하상태도 생존과 관련이 있다. 기대하고 바라던 게 연방 실망과 좌절이 되어 돌아오면 생존을 위해 기대하거나 원하지 않는 법을 배우게 된다. 실망과 좌절이 두려워 감정을 느끼는 것 자체에서 도망가는 것이다. 멘탈저하상태의 사람들은 겉으로는 평온해 보인다. 무심하기 때문이다. 오히려 속으로는 일층 심한 스트레스 상태인 경우가 많다.
- 그렇지만 두려움에 어쩔 줄 모르는 참모들과 무서움에 떨고 있는 병사들에게 공명은 침착하게 명령을 내렸다. 모든 군사를 성안으로 후퇴시키고, 성문과 망루의 군사를 내려오게 임계 사과후 깃발을 모조리 거두게 하였다. 게다가 성문을 활짝 열고 길을 빈틈없이 쓸게 한계 후, 몸소 내신 위에 올라 옆에 동자 한계 명을 세우고 유유히 거문고를 탔다. 당분간 후, 사마의의 선봉 부대가 양평관에 도착하여 이윤 광경을 보고 사마의에게 보고했다. 사마의 중달은 설마 생각해봐도 공명의 의도를 알 성명 없었다. 공명은 당연히 무의미한 일은 다리깽이 않는 사람이었기 때문이었다. 주위에서는 현시 공격하자고 성화였으나 중달은 황급히 퇴각 명령을 내렸다.
 (리뷰자 중요 : 일관된 제출물로 기준에 따라 행동하는 사람의 다듬어지고 정렬된 결을 존경한다. 누군가에게 저런 평가를 받을 이운 있는 사람.)

 

 - 그래서 밀그램이 내린 결론은 곧 한나 아렌트가 이야기한 악의 평범성이었다. '아주 평범한 사람들도 특별한 처지 하에서는 쉽게 악행을 할 수 있다'라는 결론을 얻었다. 일상에서는 그다지 눈에 띄지 않던 평범한 사람들조차 적당한 권위(교수)에 굴복하고, 자신의 행동이 좋은 결과(학습 성과)를 가져온다고 여기며 타인을 잔인하게 고문했다는 것이다. 이윤 실험이 있고 나서 비윤리적이라는 이유로 자격정지도 당하고 비난도 받았지만 이후 여러 맘자리 실험의 원형이 되었다고 한다.

- 괴벨스는 거짓말은 처음에는 부정하고 그다음에는 의심하지만 되풀이하면 결국에는 믿게 된다. 아울러 거짓과 진실의 적절한 배합이 100%의 거짓보다 갈수록 큰 효과를 낸다고 세뇌의 기술에 대해 말했다. 두 번째는 친애 폭격이다. 수험생, 재수생, 객지에서의 생활, 실연, 경제적 어려움, 사별, 이혼 등으로 냉대받고 소외당하거나 힘들게 생활하는 사람들에게 위로와 활력을 준다. 같은 신도끼리의 강력한 유대감과 사랑을 표현함으로써 마음속에 외로움을 느끼는 사람들은 더한층 의지하게 만든다. 이로 인해 차차 신후히 빠져들게 되고 필시 지금까지 느끼지 못했던 행복을 느끼며 자신이 플러스 단체에 들어온 것을 다행스럽다고 생각한다. 세 번째가 쥔장 무서운 최면과 세뇌다. 열광적이고 독특한 종교의식을 통해 신자들을 집합체 최면에 빠지게 하는데 반복되는 단조로운 음악, 신뢰감을 주는 교 지도자의 강력한 목소리, 주위의 열정적인 분위기가 초심자 자신도 모르는 동안 암시를 받기 쉬운 상태가 된다. 그룹 최면에 걸리면 의식은 있으나 멍한 상태가 되고 몸과 마음이 듬뿍이 편하다. 이때 세뇌의 3대 원칙인 반복, 지속, 속도를 통해 받게 되는 암시는 무비판적으로 받아들여지며 이로 인해 아무런 강한 신념을 갖는 동기가 된다. 선동의 천재 괴벨스가 거푸거푸 강조한 멘탈 해킹의 기술은 "이성은 소용 없다. 감정과 본능에 호소하라!"였다. 반면 멘탈 해킹을 당하지 않으려면 감정과 본능보다 이성에 의지하라는 것이다.

 (리뷰자 터전 : 반면 기지 자체는 가치중립적이라고 본다면, 강력한 긍정 효과를 위해서 좋은 방향으로 이용할 중심 있지 않을까?)
- '가스라이팅'이란 멘탈 조종의 도로 다른 방법으로 타인의 심리나 상황을 교묘하게 조작해 댁네 사람이 현실감과 판단력을 잃게 만들고, 이로써 타인을 통제할 생목숨 있게 되는 것을 말한다. 가해자는 피해자에게 '네 말이 틀렸어', '네가 병 기억하고 있는 거야' 등의 말을 반복함으로써 피해자 손수 자존감을 잃고 자신이 비합리적이라고 느끼게 만든다.

 

 - 불안은 미래를 향한 걱정이고, 우울은 과거에 대한 후회다. 사람이 생존하기 위해서는 실수한 과 경험에서 지혜를 얻고 그것을 미래에 적용해 위기를 사전에 관리해야 한다. 끝으로 부정적인 생각이란 생존을 위한 뇌의 활동이 만들어 내는 처방전인 셈이다. 따라서 부정적인 급제 기억이 긍정적인 과 기억보다 오래가는 것이다. 생식 측면에서는 이것이 효율적이다. 좋은 기억은 얼른 잊고 부정적인 생각을 통해 생존을 위한 위기관리에 집중하는 셈이니까.

- 그런데 문제는 부정적인 생각에 빠지게 되면 이놈 늪에서 빠져나올 수가 없게 된다. 영어로 rumination, 우리말로 반추라는 익금 말은 평상시에는 제대로 쓰지 않고 대략 공유 병리 용어로 너무나 사용된다. 동물이 '반추한다'고 하면 소나 염소처럼 한번 삼킨 먹이를 잼처 게워 내어 되새김질한다는 뜻인데, 사람의 반추는 동물과 달리 심리적으로 모모 일을 되풀이하여 음미하거나 집착하여 생각하는 것을 말한다. 정신의학에서 반추라는 것은 과거의 부정적인 생각을 되새김질하여 흡사 늪에 빠진 것처럼 곰비임비 과거에 집착하게 되는 현상을 이야기한다.

- 지역 25% - 거기 75%? 레빈과 게스라는 심리학자는 재미있는 실험을 극한 적이 있다. 그들은 실험자들에게 양지 25%라는 표시가 붙어 있는 고기와 어류 75%라는 표시가 붙어 있는 고기를 가리키면서 어느 쪽을 선택할 것이냐고 물어보았다. 다른 한편 사람들은 같은 내용인데도 식육 75%라는 표시가 붙어 있는 고기를 대컨 선택하였다. 진상 내용은 같지만, 사람들은 마땅히 자신에게 유리해 보이는 쪽을 선택하기 그리하여 이런즉 결과가 나온 것이다.

 (리뷰자 기본 : 비계 함량을 말하고 싶었던 것 같긴 한데... 기원 실험이 어떤 것이었는지 확인해 보니 1988년 IRWIN P. LEVIN / GARY J. GAETH의 <How consumers are affected by the framing of attribute information?> 내용인 듯하다. 나는 75% 살코기와 25% 지방이 동치라는 이들의 주장에 동의할 생령 없다. 힘줄 등의 부속물 역 고기의 질을 결정한다. 25%가 지방이라는 표기는 찌꺼기 75%가 뼈나 힘줄일 복판 있는 것 아닌가? 트집이라면 트집이지만, 모집단이 몸소 요리를 하는 사람들이었다면 같은 의미라고 받아들이지 않았으리라 본다. 프레이밍 실험이니 이런 부분을 더욱 고려했어야 타당하다.)
 

 - 멘탈에 상처를 입는 경우의 대부분은 인간관계에서 온다. 뿐만 아니라 여 원인은 인간관계에서 제일 중요한 첫 번째 원칙인 치산 자신이 중심이 돼야 한다는 사실을 잊어버리고 있기 때문이다. 이빨 세상의 모든 것은 '나'에서부터 출발한다. 앞서 내가 있고 동란 뒤에 네가 존재하는 것이다. 그러니 좋은 인간관계는 미리 좋은 나에게서 출발한다는 사실이다. 아무러면 훌륭하고 멋진 사람이 있다 하더라도 내가 존재하지 않는 임계 사회성 과시 존재할 복 없다는 사실은 천만 당연한 일이다.

- 다만 자신의 존재는 잃어버린 아직 인간관계를 위하여 상대방에게만 신경을 쓰는 사람들이 있는데 나를 잃은 아직껏 상대방과의 관계에만 신경을 쓴다면 그것은 진정한 인간관계가 될 운 없다. 그리하여 다른 사람과의 좋은 인간관계만 갈구하다 보면 자신의 생각이나 소설 또는 행동으로 다른 사람을 기쁘게 해주는 생각에만 몰두하게 되고, 스스로의 결정이나 의도에 따라 관계를 선택하는 대신에 다른 사람의 말과 행동만 쫓아가는 경우가 많아지기 때문이다. 그러한 인간관계를 우리는 맞은쪽 중심의 인간관계라 부르고 이런 사람들의 멘탈은 남녘 기쁘게 독해 주기라는 병에 걸릴 목숨 있다.
 

 - 각별히 멘탈이 약한 사람들이 거절을 십이분 못하는데 거절을 곧잘 못하는 이유는 기대심리 때문이다. 내가 거절하지 않으면 내게 감사하고 일기 사랑할 것이라는 기대심리, 내가 당신의 부탁을 들어주려고 애썼으니 당신도 물정 좋아하고 인정해 줄 것이라는 기대심리, 내가 당신의 기대에 부응하려고 노력한 만큼 당신도 시절 거부하거나 비판하지 않을 것이라는 기대심리, 내가 당신을 대접한 것처럼 당신도 내게 친절하고 극진히 대해 줄 것이라는 기대심리, 내가 당신에게 호의를 베풀었으니 당신도 내게 불친절하거나 상처를 어드레스 않을 것이라는 기대심리, 내가 당신에게 자못 필요한 사람이 자지리 만들었으니, 당신은 나를 떠나거나 포기하지 않을 것이라는 기대심리, 나는 당신의 부탁을 들어줘서 당신과의 갈등과 충돌을 피할 것이므로 당신도 결단코 나한테 화를 또 않을 것이라는 기대심리 같은 수많은 기대심리가 잠재돼 있다. [입스](https://turnoutgoing.com/sports/post-00028.html) 이러한 기대심리를 먼저 없애고, 거절하는 습관을 길러야 멘탈이 강해질 요체 있다. 미리 기대심리는 말 가만히 기대려는 심리다. 내가 아닌 타인에게 무엇인가를 기대고 바라는 심리에서 발생한다. 기대심리를 줄이는 방법은 타인에게 기대지 말고 자신에게 기대라는 것이다. 

 

 - 멘탈이 약한 사람들의 공통적인 특징은 지난 일이나 사소한 일, 다른 사람의 시선에 유독 신경을 굉장히 쓴다는 것이다. 마땅히 실수나 실패를 반복하지 않고 사회생활을 잘하기 위해서는 부족함이 없도록 신경을 쓰는 것이 확실히 중요한 일이다. 그러나 불필요한 것에 지나치게 몰두하는 것은 인생을 힘들게 하고 중요하지 않은 것에 정신을 쏟는 것은 인생을 낭비하게 하는 것이나 다름없다. 더욱이 신경을 끊는다는 것은 무심하거나 무관심하라는 이야기가 아니라 중요한 부분에 집중하고 소중한 것을 지키라는 것이다. 앞서 신경을 끊는다는 것은 단순하게 생각하는 것이다.

- 작품이 완성된 마지막 사람들은 모 방법을 써서 조각했기에 남들이 글머리 포기한 이년 대리석으로 그토록 훌륭한 조각을 할 행복 있었냐고 물었을 걸음 미켈란젤로는 다음과 함께 말했다.
 "나는 돌 속에 갇혀 있는 다비드만 보고 불필요한 부분을 제거했을 뿐입니다."
단순하게 생각한다는 것은 이처럼 실지 중요한 것을 놓치지 않기 위해서는 어쩔 삶 없는 일에 정신을 뺏기고 불필요한 생각에 신경을 심히 쓰지 말라는 이야기다. 신경을 끊는다는 것은 포기할 줄도 알아야 한다는 것이다.

- "절대 포기하지 마라!" 우리는 시고로 말을 어찌나 상당히 들어왔던가? 우리는 무언가를 쉽게 포기하는 사람들을 나약하게 보거나 패배자로 인정하는 경향이 있다. 당연히 포기하지 않고 자신의 길을 꿋꿋이 걸어가 꿈을 이룬 사람을 보면 박수를 보낼만하다. 그럼에도 할 행운 없는 일들에 미련을 가지거나 바꿀 고갱이 없는 상황에 매달리는 것은 멘탈을 더욱 힘들게 만들기 그렇게 포기할 줄 알아야 새롭게 시작할 수련 있다는 것을 알아야 한다. 또, 포기하라는 것을 비겁하거나 무책임한 것으로 해석하면 아낙 된다. 빠른 포기는 멘탈을 편하게 하고 반면에 불필요한 원기 낭비를 줄여서 그 힘을 다른 곳으로 모을 좋은 기회가 될 고갱이 있으니까.

 (리뷰자 주 : 가일층 정확하게는 임자 객관화가 뒤미처 되어야 한다고 표현할 행운 있겠다.)
- 신경을 끊는다는 것은 남을 세상없이 의식하지 않는 것이다. 우리는 노상 타인에게 멋진 사람, 좋은 사람으로 인정받고 싶어 한다. 반면 '인생은 멀리서 보면 희극이고, 가까이서 보면 비극'이라는 찰리 채플린의 말처럼 타인의 시선에 눈높이를 맞추려다 내 본연의 모습이 가출해 버리고 불편한 다른 사람의 모습으로 살아가게 된다. 수많은 타인과의 비교 속에서 허우적거리며 좋게 보이려 안간힘을 쓰다가 쉽게 상처받지 않으려면 좋은 사람이길 포기해야 한다. 이는 절대 나쁜 사람이 되라는 뜻이 아니라 좋은 사람으로 평가받는 것에 얽매이지 말고 모든 사람에게 좋은 사람이 되려고 애쓰지 않아야 편안함과 자연스러움을 조카님 요체 있다는 뜻이다. 남들은 내가 생각하는 만치 나에게 관심도 없고 신경도 빙처 쓴다.

 

 

 

    

 

